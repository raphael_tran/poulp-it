package content;

import core.PlayerInput;
import core.annotations.Unused;

/**
 * Superclass for all scripts which can be added to a GameObject.
 * A script describes the behaviour of a GameObject and is updated each frame.
 *
 * @author Raph, inspired by the functioning of the Unity Engine.
 *
 */
public abstract class MonoBehaviour {

	/**
	 * The GameObject to which the script is attached
	 */
	public GameObject support;




	/**
	 * This method is called once, when a GameObject with this script is instantiated.
	 *
	 */
	public void start() { }


	/**
	 * This methods is called every frame the support GameObject is active (which IS every frame for now).
	 *
	 * @param deltaTime 			the time in seconds it took to complete the last frame
	 * @param previousPlayerInput
	 */
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) { }


	/**
	 * Same as update but the lateUpdate methods are called after all updates are done.
	 * Not functional so far.
	 *
	 */
	@Unused
	public void lateUpdate() { }





}
