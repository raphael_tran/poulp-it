package content;

/**
 * Interface implemented by any object which does something in the game without being a graphical element.
 * Any such object must override the apply() method, which will be called each frame by the GameEngine.
 * Example: the object which manages the spawn of collectables.
 * In practice, it is quite similar to GameObjects and their update method, but allows for a semantic differentiation.
 *
 * @author Raph
 *
 */
public interface GameManager {

	/**
	 * Called every frame.
	 *
	 */
	void apply();


}
