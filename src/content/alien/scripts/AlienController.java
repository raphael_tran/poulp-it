package content.alien.scripts;

import content.GameObject;
import content.MonoBehaviour;
import content.alien.Alien;
import content.alien.Pineapple;
import core.GameEngine;
import core.Launcher;
import core.PlayerInput;
import core.util.Vector2;

/**
 * The script which control the alien.
 * FIXME not totally accurate, but I don't have the time to fix it
 *
 * @author Matthias
 *
 */
public class AlienController extends MonoBehaviour {

	Vector2 speed = Vector2.ZERO();
	private float maxX = (float) Launcher.mainWindow.getWidth();
	private float maxY = (float) Launcher.mainWindow.getHeight();
	private float speedFactor = .5f;



	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput, float maxX, float maxY) {
		this.maxX = maxX;
		this.maxY= maxY;
		update(deltaTime, playerInput, previousPlayerInput);
	}

	@Override
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) {

		changeSpeed(playerInput.directionalInput);
		support.position.x += speed.x;
		support.position.y += speed.y;
		validatePosition(maxX, maxY);

		// Check collisions with EVERYTHING !!!
		for (GameObject gameObject: GameEngine.allGameObjects) {		// LOL sinful
			if (gameObject instanceof Pineapple) {
				Pineapple pineapple = (Pineapple) gameObject;
				if (intersects(pineapple)) {
					GameEngine.remove(pineapple);
				}
			}
		}
	}

	public void validatePosition(float maxX, float maxY) {
		float x = support.position.x;
		float y = support.position.y;

		if (x + Alien.SPRITE_WIDTH >= maxX) {
			x = maxX - Alien.SPRITE_WIDTH;
			speed.x *= -1;
		} else if (x < 0) {
			x = 0;
			speed.x *= -1;
		}

		if (y + Alien.SPRITE_HEIGHT >= maxY) {
			y = maxY - Alien.SPRITE_HEIGHT;
			speed.y *= -1;
		} else if (y < 0) {
			y = 0;
			speed.y *= -1;
		}
	}

	public void changeSpeed(Vector2 directionalInput) {
		speed.x  += directionalInput.x * speedFactor;
		speed.y  += directionalInput.y * speedFactor;
	}

	public boolean intersects(Pineapple other) {
		float x = support.position.x;
		float y = support.position.y;

		return ((x >= other.position.x && x <= other.position.x + other.SPRITE_WIDTH) ||
				(other.position.x >= x && other.position.x <= x + Alien.SPRITE_WIDTH)) &&
				((y >= other.position.y && y <= other.position.y + other.SPRITE_HEIGHT) ||
						(other.position.y >= y && other.position.y <= y + Alien.SPRITE_HEIGHT));

	}


}
