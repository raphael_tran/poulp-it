package content.alien.scripts;

import content.MonoBehaviour;
import content.alien.Alien;
import core.Launcher;
import core.PlayerInput;
import core.util.Vector2;

/**
 * The script which control a pineapple.
 * FIXME
 *
 * @author Matthias
 *
 */
public class PineappleController extends MonoBehaviour {

	/**
	 * The maximum value a coordinate of the velocity can have; must be positive.
	 */
	private float maxVelocity = 5;


	/**
	 * The velocity of the pineapple at a given time.
	 */
	private Vector2 velocity;
	private float maxX = Launcher.mainWindow.getWidth();
	private float maxY =  Launcher.mainWindow.getHeight();



	@Override
	public void start() {
		updateVelocity();

	}


	@Override
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) {

		// Direction change
		if (Math.random() > 0.995) {
			updateVelocity();
		}

		// Movement
		support.position.x += velocity.x;
		support.position.y += velocity.y;
		validatePosition(maxX, maxY);
	}


	public void validatePosition(float maxX, float maxY) {
		float x = support.position.x;
		float y = support.position.y;

		if (x + Alien.SPRITE_WIDTH >= maxX) {
			x = maxX - Alien.SPRITE_WIDTH;
			velocity.x *= -1;
		} else if (x < 0) {
			x = 0;
			velocity.x *= -1;
		}

		if (y + Alien.SPRITE_HEIGHT >= maxY) {
			y = maxY - Alien.SPRITE_HEIGHT;
			velocity.y *= -1;
		} else if (y < 0) {
			y = 0;
			velocity.y *= -1;
		}
	}

	/**
	 * Change the velocity by drawing uniformly two random values in [-maxVelocity; maxVelocity] for both its coordinates.
	 */
	public void updateVelocity() {
		velocity = new Vector2(
				maxVelocity * ((float) Math.random() - 0.5f),
				maxVelocity * ((float) Math.random() - 0.5f)
		);
	}


}
