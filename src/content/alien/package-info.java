/**
 * The content related to Alien VS Pineapple.
 *
 * The gameplay is intentionally bad as the goal was to recreate something made in a java lesson.
 * Our goal with demo was to showcase the efficiency of our engine to produce a working prototype.
 *
 * Here is the link to the original lesson (in french):
 * http://www-inf.telecom-sudparis.eu/COURS/CSC3101/Supports/?page=exercices/ci9&wrap=true&soluce=true
 *
 * The folder "original" contains the code that was written in this 3h class.
 */
package content.alien;