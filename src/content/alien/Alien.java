package content.alien;

import content.GameObject;
import content.Layer;
import content.Tag;
import content.alien.scripts.AlienController;
import core.Launcher;
import core.util.BoxCollider;
import core.util.Vector2;
import javafx.scene.image.Image;

/**
 * ---
 *
 * @author Matthias
 *
 */
public class Alien extends GameObject {

	static final String SPRITE_PATH = Launcher.RESOURCES_URI + "graphic/alien/" + "alien.png";

	public static final float SPRITE_WIDTH = 62;
	public static final float SPRITE_HEIGHT = 36;



	public Alien(Vector2 position, Layer layer, Tag tag) {
		super(
				position,
				new Image(SPRITE_PATH, SPRITE_WIDTH, SPRITE_HEIGHT, false, false),
				layer,
				tag,
				new BoxCollider(SPRITE_WIDTH, SPRITE_HEIGHT),

				new AlienController()
		);

	}



}