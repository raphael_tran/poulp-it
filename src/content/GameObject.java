package content;

import core.PlayerInput;
import core.annotations.Unused;
import core.debug.Debug;
import core.util.Collider;
import core.Renderable;
import core.util.Vector2;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.LinkedList;

/**
 * Superclass for any game element in a scene which has a spatial position and can be visually rendered.
 * Example: a player
 *
 * @author Raph
 *
 */
public abstract class GameObject implements Renderable {

	/**
	 * The current position in global coordinates of this GameObject
	 */
	public Vector2 position;

	/**
	 * The sprite used for rendering this GameObject
	 */
	public Image sprite;

	/**
	 * The layer on which the GameObject is
	 */
	@Unused
	public Layer layer;

	/**
	 * A tag associated with this GameObject; used for convenient references between the different objects
	 */
	public Tag tag;

	/**
	 * The collider used to detect collisions against this GameObject; null if this GameObject can't be collided with
	 */
	public Collider collider;

	/**
	 * A list of scripts attached to this GameObject, which describe its behaviour
	 */
	public LinkedList<MonoBehaviour> scripts;



	/**
	 * Standard constructor for a GameObject.
	 * All scripts are initialized at the end of this instantiation.
	 *
	 * @param position		the position in global coordinates where the GameObject will spawn
	 * @param sprite		the image which will represent the GameObject
	 * @param layer
	 * @param tag
	 * @param collider		the Collider of the GameObject; set null if the GameObject can't be collided with
	 * @param scriptsGiven	the scripts attached to this GameObject
	 */
	public GameObject(
			Vector2 position,
			Image sprite,
			Layer layer,
			Tag tag,
			Collider collider,
			MonoBehaviour... scriptsGiven
	) {

		this.position = position;
		this.sprite = sprite;
		this.layer = layer;
		this.tag = tag;
		this.collider = collider;

		this.scripts = new LinkedList<>();
		for (MonoBehaviour script: scriptsGiven) {
			// Setting the right support reference for the script
			script.support = this;

			this.scripts.add(script);
		}

		// Starting all scripts
		// Note that this loop MUST come after the previous one so that script can reference each other in their start()
		for (MonoBehaviour script: this.scripts) {
			script.start();
		}

	}



	/**
	 * The update method called by the GameEngine.
	 * By default, it simply updates all the scripts attached to this.
	 * This method can be overridden for a more specific behaviour.
	 *
	 * @param deltaTime		the time in seconds it took to complete the last frame
	 * @param playerInput
	 * @param previousPlayerInput
	 */
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) {
		updateAllScripts(deltaTime, playerInput, previousPlayerInput);
	}

	/**
	 *
	 * @param deltaTime		the time in seconds it took to complete the last frame
	 * @param playerInput
	 */
	protected final void updateAllScripts(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) {
		for (MonoBehaviour script: scripts) {
			script.update(deltaTime, playerInput, previousPlayerInput);
		}
	}


	/**
	 * Translate this GameObject by the given amount.
	 */
	public void translate(float xTranslation, float yTranslation) {
		this.position.x += xTranslation;
		this.position.y += yTranslation;
	}



	/**
	 * Render this GameObject on the GraphicsContext gc.
	 */
	@Override
	public final void render(GraphicsContext gc, Vector2 renderPosition) {
		gc.drawImage(sprite, renderPosition.x, gc.getCanvas().getHeight() - renderPosition.y);
		if (Debug.DEBUG_ENABLED) {
			if (collider != null) {
				collider.render(gc, renderPosition);
			}
		}
	}

	/**
	 * Convenient method which render this GameObject directly at its current position.
	 *
	 * @param gc	the GraphicsContext on which this will be rendered
	 */
	public void render(GraphicsContext gc) {
		render(gc, position);
	}




	@Override public String toString() {
		return "GameObject [Position " + this.position + "; Collider " + collider + "]";
	}



	/**
	 * This method generates the list of scripts which will be attached to a GameObject.
	 * A GameObject must override this method in order to set its own scripts.
	 * We use such an implementation to bypass the (annoying) fact that Java won't allow any instruction before super()
	 * in a constructor.
	 *
	 * @return		an empty list by default (if not overridden)
	 */
	@Deprecated
	protected static LinkedList<MonoBehaviour> generateScriptsList() {
		return new LinkedList<>();
	}



}
