package content.maze;

import content.GameObject;
import content.Layer;
import content.Tag;
import core.PlayerInput;
import core.util.Collider;
import core.util.Vector2;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The GameObject for the player in the maze game.
 * The Maze in which the player evolve is actually an attribute of this GameObject.
 * The Player is represented as a circle in the maze.
 *
 * @author Raph
 *
 */
public class PlayerMaze extends GameObject {

	/**
	 * The amount of time the player cannot move after moving once.
	 * Used to avoid going too fast at the end of a corridor.
	 */
	public static float TIME_BETWEEN_TWO_STEPS = 0.04f;

	/**
	 * The color of the player
	 */
	public static Color PLAYER_COLOR = Color.MAGENTA;

	/**
	 * The ratio of the player dimensions compared to one "tile"'s dimensions
	 */
	public static float PLAYER_TILE_RATIO = 0.5f;
	private static float K = (1 - PLAYER_TILE_RATIO) / 2;



	public int x;
	public int y;

	private Maze maze;		// Yes, the maze object is actually included in the player object

	private float moveCooldown = 0.1f;		// The time left before the player is able to move again



	public PlayerMaze() {
		super(null, null, Layer.DEFAULT, Tag.DEFAULT, Collider.NO_COLLIDER);

		maze = new Maze();
	}

	public PlayerMaze(int mazeWidth, int mazeHeight) {
		this(mazeWidth, mazeHeight, false);
	}

	public PlayerMaze(int mazeWidth, int mazeHeight, boolean mazeIsFantastic) {
		super(null, null, Layer.DEFAULT, Tag.DEFAULT, Collider.NO_COLLIDER);

		maze = new Maze(mazeWidth, mazeHeight, mazeIsFantastic);
	}



	@Override
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput) {


		// BAD!
		if (moveCooldown <= 0) {		// if we're not in move cooldown
			float xInput = Vector2.dotProduct(playerInput.directionalInput, Vector2.RIGHT());
			float yInput = Vector2.dotProduct(playerInput.directionalInput, Vector2.UP());

			if (xInput != 0 && yInput == 0) {
				int direction = (int) Math.signum(xInput);
				if (maze.canMoveLeftRight(x, y, direction)) {
					x += direction;
					moveCooldown = TIME_BETWEEN_TWO_STEPS;
				}
			} else if (xInput == 0 && yInput != 0) {
				int direction = (int) Math.signum(yInput);
				if (maze.canMoveUpDown(x, y, direction)) {
					y -= direction;
					moveCooldown = TIME_BETWEEN_TWO_STEPS;
				}
			}
		} else {
			moveCooldown -= deltaTime;
		}

	}





	/**
	 * We override the GameObject render method because the player is not represented by a specific sprite but is
	 * simply a full circle which has to be adapted to the size of the maze.
	 * Also, since the maze is "part" of the player, its rendering is called here.
	 */
	@Override
	public void render(GraphicsContext gc) {

		maze.render(gc);


		double positionX = maze.x0 + (x + K) * maze.wallSize;
		double positionY = maze.y0 + (y + K) * maze.wallSize;

		double playerScale = PLAYER_TILE_RATIO * maze.wallSize;

		gc.setFill(PLAYER_COLOR);
		gc.fillOval(positionX, positionY, playerScale, playerScale);
	}



	@Override public String toString() {
		return "PlayerMaze[" + x + ", " + y + "]";
	}

}
