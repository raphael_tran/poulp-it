package content.maze;

import core.Renderable;
import core.util.Vector2;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a rectangular maze.
 * It is characterized by two matrix : horizontalWalls and verticalWalls.
 *
 * @author Raph
 *
 */
public class Maze implements Renderable {

	/* STATIC VARIABLES */

	// Construction related variables
	private static final int DEFAULT_WIDTH = 38;		// | (38, 17) optimal for my Eclipse console
	private static final int DEFAULT_HEIGHT = 17;		// |
	// Representation related variables
	private static final Color WALL_COLOR = Color.WHITE;
	private static final float WALL_WIDTH = 1;
	private static final Color BACKGROUND_COLOR = Color.BLACK;
	// Other (please do not touch, they're fine like that)
	private static final boolean ZEROZERO = false;		// Does the recursion start at (0, 0)?
	private static final float MARGIN = 0.02f;		// Used for window representation	@@@


	// ATTRIBUTES

	public boolean fantasticMode;		// Wondering what this is? Well, try turning it on ;) (excepted if you're epileptic)
	public double x0;
	public double y0;
	public double wallSize;


	private int width;
	private int height;
	private boolean[][] horizontalWalls;		// | false means there is a wall; true means there isn't one
	private boolean[][] verticalWalls;			// |

	private boolean[][] visited;		// Used by intern methods: true means visited



	public Maze() {
		this(DEFAULT_WIDTH, DEFAULT_HEIGHT, false);
	}

	public Maze(int width, int height) {
		this(width, height, false);
	}

	public Maze(int width, int height, boolean fantasticMode) {
		this.width = width;
		this.height = height;
		this.fantasticMode = fantasticMode;
		this.horizontalWalls = new boolean[width][height+1];
		this.verticalWalls = new boolean[width+1][height];
		this.visited = new boolean[width][height];

		if (ZEROZERO) {
			dig(0, 0);
		} else {
			dig(width/2, height/2);
		}
	}


	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}



	/**
	 * Indicates if it is possible to move left or right from the tile (x, y).
	 */
	public boolean canMoveLeftRight(int x, int y, int direction) {
		return verticalWalls[direction<0 ? x : x+1][y];
	}

	/**
	 * Indicates if it is possible to move up or down from the tile (x, y).
	 */
	public boolean canMoveUpDown(int x, int y, int direction) {
		return horizontalWalls[x][direction<0 ? y+1 : y];
	}





	/**
	 * Calculate the length of a wall according to the size of the rendering window;
	 * also calculate the origin of the maze in the window.
	 *
	 * @param windowWidth	the width of the window the maze will be rendered on
	 * @param windowHeight	the height of the window the maze will be rendered on
	 */
	public void calculate(double windowWidth, double windowHeight) {
		if (windowWidth * height < windowHeight * width) {		// Is the maze large or long?
			x0 = windowWidth * MARGIN;
			wallSize = (windowWidth - 2*x0) / width;
			y0 = (windowHeight - height*wallSize) / 2;
		} else {
			y0 = windowHeight * MARGIN;
			wallSize = (windowHeight - 2*y0) / height;
			x0 = (windowWidth - width*wallSize) / 2;
		}
	}



	/**
	 * The overridden render method.
	 * @param position is unused here because a maze fills the window.
	 */
	@Override
	public void render(GraphicsContext gc, Vector2 position) {

		// This enables to rescale the window dynamically (almost...)
		double windowWidth = gc.getCanvas().getScene().getWidth();
		double windowHeight = gc.getCanvas().getScene().getHeight();

		gc.setFill(BACKGROUND_COLOR);
		gc.fillRect(0, 0, windowWidth, windowHeight);

		calculate(windowWidth, windowHeight);

		// Maze representation
		gc.setLineWidth(WALL_WIDTH);		// Width of the walls
		gc.setStroke(WALL_COLOR);
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				if (!horizontalWalls[i][j]) {
					if (fantasticMode) {
						gc.setStroke(AnnexMaze.randomColor());
					}
					double x = x0 + i*wallSize;
					double y = y0 + j*wallSize;
					gc.strokeLine(x, y, x+wallSize, y);
				}
			}
			for (int i = 0; i < width; i++) {
				if (!verticalWalls[i][j]) {
					if (fantasticMode) {
						gc.setStroke(AnnexMaze.randomColor());
					}
					double x = x0 + i*wallSize;
					double y = y0 + j*wallSize;
					gc.strokeLine(x, y, x, y+wallSize);
				}
			}
		}
		gc.setStroke(WALL_COLOR);
		double w = width*wallSize;
		double h = height*wallSize;
		gc.strokeRect(x0, y0, w, h);		// Contour
	}


	public void render(GraphicsContext gc) {
		// The rendering position is unused for the maze, which fill the window, thus:
		render(gc, null);
	}










	/**
	 * Return a ListArray indicating the adjacent tiles accessible; uses visited matrix.
	 * Used for generation: DOES NOT TAKE WALLS INTO ACCOUNT
	 *
	 * 0 -> tile above
	 * 1 -> tile to the right
	 * 2 -> tile below
	 * 3 -> tile to the left
	 */
	private List<Integer> neighbors0(int x, int y) {
		List<Integer> result = new ArrayList<>();		//
		if (y != 0 && !visited[x][y-1]) {		// The tile above
			result.add(0);
		}
		if (x != width-1 && !visited[x+1][y]) {		// The tile on the right
			result.add(1);
		}
		if (y != height-1 && !visited[x][y+1]) {		// The tile below
			result.add(2);
		}
		if (x != 0 && !visited[x-1][y]) {		// The tile on the left
			result.add(3);
		}
		return result;
	}

	/** Return the adjacent tiles to this one, considering walls.
	 *
	 * 0 -> tile above
	 * 1 -> tile to the right
	 * 2 -> tile below
	 * 3 -> tile to the left
	 */
	private List<Integer> neighbors1(int x, int y) {
		List<Integer> result = new ArrayList<>();		//
		if (horizontalWalls[x][y]) {		// The tile above
			result.add(0);
		}
		if (verticalWalls[x+1][y]) {		// The tile on the right
			result.add(1);
		}
		if (horizontalWalls[x][y+1]) {		// The tile below
			result.add(2);
		}
		if (verticalWalls[x][y]) {		// The tile on the left
			result.add(3);
		}
		return result;
	}

	/** Return the adjacent tiles to this one, considering walls and visited tiles.
	 *
	 * 0 -> tile above
	 * 1 -> tile to the right
	 * 2 -> tile below
	 * 3 -> tile to the left
	 */
	private List<Integer> neighbors2(int x, int y) {
		List<Integer> result = new ArrayList<>();		//
		if (horizontalWalls[x][y] && !visited[x][y-1]) {		// The tile above
			result.add(0);
		}
		if (verticalWalls[x+1][y] && !visited[x+1][y]) {		// The tile on the right
			result.add(1);
		}
		if (horizontalWalls[x][y+1] && !visited[x][y+1]) {		// The tile below
			result.add(2);
		}
		if (verticalWalls[x][y] && !visited[x-1][y]) {		// The tile on the left
			result.add(3);
		}
		return result;
	}

	/**
	 * A convenient method which translate the "neighbor code" n to actual coordinates.
	 * Example: 2 -> {x, y+1})
	 */
	private int[] toCoordinates(int n, int x, int y) {
		switch (n) {
		case 0:
			return new int[] {x, y-1};
		case 1:
			return new int[] {x+1, y};
		case 2:
			return new int[] {x, y+1};
		case 3:
			return new int[] {x-1, y};
		default:
			throw new Error("Should not happen");
		}
	}


	/**
	 * "Dig" the initially walled maze (depth-first).
	 *
	 * @param x		the x-coordinates of the starting point of the algorithm
	 * @param y		the y-coordinates of the starting point of the algorithm
	 */
	private void dig(int x, int y) {
		visited[x][y] = true;
		List<Integer> neighbors = neighbors0(x, y);

		while (!neighbors.isEmpty()) {

			int index = AnnexMaze.randInt(0, neighbors.size());
			int newX = x;
			int newY = y;

			switch (neighbors.get(index)) {
			case 0:		// We go up
				newY--;
				horizontalWalls[x][y] = true;
				break;
			case 1:		// We go right
				newX++;
				verticalWalls[x+1][y] = true;
				break;
			case 2:		// We go down
				newY++;
				horizontalWalls[x][y+1] = true;
				break;
			case 3:		// We go left
				newX--;
				verticalWalls[x][y] = true;
				break;
			default:
			}
			dig(newX, newY);
			// It's important to refresh neighbors as the previous line changed the maze structure
			neighbors = neighbors0(x, y);
		}
	}



	@Override public String toString() {
		/* PFIOUUU! */

		// First two lines
		StringBuilder result = new StringBuilder("┌───");
		for (int i = 1; i < width; i++) {
			if (verticalWalls[i][0]) {
				result.append("─");
			} else {
				result.append("┬");
			}
			result.append("───");
		}
		result.append("┐");
		result.append(System.lineSeparator());
		result.append("│   ");
		for (int i = 1; i < width; i++) {
			if (verticalWalls[i][0]) {
				result.append(" ");
			} else {
				result.append("│");
			}
			result.append("   ");
		}
		result.append("│");
		result.append(System.lineSeparator());

		// Middle lines
		for (int j = 1; j < height; j++) {
			if (horizontalWalls[0][j]) {
				result.append("│   ");
			} else {
				result.append("├───");
			}
			for (int i = 1; i < width; i++) {
				result.append(intersection(verticalWalls[i][j - 1], horizontalWalls[i][j],
						verticalWalls[i][j], horizontalWalls[i - 1][j]));
				if (horizontalWalls[i][j]) {
					result.append("   ");
				} else {
					result.append("───");
				}
			}
			if (horizontalWalls[width-1][j]) {
				result.append("│");
			} else {
				result.append("┤");
			}
			result.append(System.lineSeparator());

			result.append("│   ");
			for (int i = 1; i < width; i++) {
				if (verticalWalls[i][j]) {
					result.append(" ");
				} else {
					result.append("│");
				}
				result.append("   ");
			}
			result.append("│");
			result.append(System.lineSeparator());
		}

		// Last line
		result.append("└───");
		for (int i = 1; i < width; i++) {
			if (verticalWalls[i][height-1]) {
				result.append("─");
			} else {
				result.append("┴");
			}
			result.append("───");
		}
		result.append("┘");

		return result.toString();
	}

	/**
	 * Return the correct character according to the nearby walls; used in toString.
	 *
	 * @param up		is there a wall to the up?
	 * @param right		is there a wall to the right?
	 * @param down		is there a wall to the down?
	 * @param left		is there a wall to the left?
	 * @return			the correct ASCII character of the wall intersection
	 */
	private String intersection(boolean up, boolean right, boolean down, boolean left) {
		switch ((up ? 8:0) + (right ? 4:0) + (down ? 2:0) + (left ? 1:0)) {
		case 0:
			return "┼";
		case 1:
			return "├";
		case 2:
			return "┴";
		case 3:
			return "└";
		case 4:
			return "┤";
		case 5:
			return "│";
		case 6:
			return "┘";
		case 7:
			return "╵";
		case 8:
			return "┬";
		case 9:
			return "┌";
		case 10:
			return "─";
		case 11:
			return "╶";
		case 12:
			return "┐";
		case 13:
			return "╷";
		case 14:
			return "╴";
		case 15:
			return "·";
		default:
		}
		throw new Error("Should not happen");
	}



}
