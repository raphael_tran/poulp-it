/**
 * The content related to the rhythm game.
 * Keys among A, Z, E and R are going to appear on the screen. You need to have them pressed when they disappear.
 * Try to reach 100 points!
 */
/**
 * @author Raph
 *
 */
package content.rhythm_game;