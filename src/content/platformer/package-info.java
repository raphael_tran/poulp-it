/**
 * The content related to the multiplayer platformer game.
 * Some code is adapted from some C# script written by Sebastian Lague for Unity.
 * See https://www.youtube.com/playlist?list=PLFt_AvWsXl0f0hqURlhyIoAabKPgRsqjz for the original content.
 */
/**
 * @author Raph
 *
 */
package content.platformer;