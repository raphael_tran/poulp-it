/**
 * The content related to the top view shooter.
 * Currently we can't really shoot much, but we can move!
 * The level loaded is for decoration only, no collision has been programmed yet.
 */
/**
 * @author Raph
 *
 */
package content.shooter;