package core;

/**
 * This class represents a graphical window.
 *
 * @author Raph
 */
public class Window {


	private int width;

	private int height;

	boolean resizable;


	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean isResizable() {
		return resizable;
	}



	public Window(int width, int height, boolean resizable) {
		this(width, height, resizable, 1);
	}


	public Window(int width, int height, boolean resizable, double scale) {
		this.width = (int) (scale * width);
		this.height = (int) (scale * height);
		this.resizable = resizable;
	}


}
