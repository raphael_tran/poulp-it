package core;

import content.GameObject;
import core.debug.DebugRenderable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import levels.Tile;

/**
 * Client side.
 * This class communicates with the GameEngine on the server, and draw on the client' screen with the received data.
 *
 * @author Raph
 * @author Matthias
 *
 */
public class GraphicManager {


	public static Image background = null;



	/**
	 * Display the game at its current state by rendering everything needed.
	 *
	 * @param gc			- the GraphicContext on which the rendering will be done
	 */
	public void renderGame(GraphicsContext gc) {

		double windowWidth = gc.getCanvas().getWidth();
		double windowHeight = gc.getCanvas().getHeight();

		// Rendering background
		if (background != null) {
			gc.drawImage(background, 0, 0, Launcher.mainWindow.getWidth(), Launcher.mainWindow.getHeight());

		} else {
			// Clearing the window
			gc.clearRect(0, 0, Launcher.mainWindow.getWidth(), Launcher.mainWindow.getHeight());
		}

		// Rendering level
		if (GameEngine.level != null) {		// if the game uses a level
			for (Tile tile: GameEngine.level.tileList) {		// XXX tileList = ugly
				tile.render(gc);
			}
		}

		// Rendering GameObjects
		for (GameObject gameObject: GameEngine.allGameObjects) {
			System.out.println("Rendering GameObject " + gameObject + " on " + gameObject.position);
			gameObject.render(gc);
		}


		// Rendering debug elements
		for (DebugRenderable gizmo: GameEngine.debugElements) {
			gizmo.render(gc);
		}


	}



}
