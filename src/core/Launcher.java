package core;

import core.util.GameInformation;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;

/**
 * This is the starting point of the program.
 * Launcher extends Application and thus has a start method called.
 * This class should stay relatively clean and only call other methods.
 *
 * @author Raph
 * @author Matthias
 *
 */
public class Launcher extends Application {


	/**
	 * The game that will be loaded
	 */
	static Game game = Game.ALIEN;



	/**
	 * The program is launched in a window with the same dimension as the screen, scaled by this number.
	 */
	public static final double WINDOW_SCALE = 0.9;


	public static final boolean FRAMERATE_DISPLAYED = true;


	/**
	 * The URI of the resources directory. It should be used whenever a class needs to load an asset.
	 */
	public static final URI RESOURCES_URI = Paths.get(System.getProperty("user.dir") + "/resources/").toUri();



	public static Window mainWindow;



	private PlayerInput previousPlayerInput;



	public static void main(String[] args) {
		/*
		  When the Application is launched,
		  - init() is called
		  - start() is called
		  - waiting for Platform.exit() or last window closed
		  - stop() is called
		 */
		System.out.println("Starting the program.");
		launch(args);
	}



	@Override
	public void init() {
		Paths.get(System.getProperty("user.dir") + "/resources/").toUri();
	}



	@Override
	public void start(Stage stage) throws IOException {

		// Initialization of the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();		// Note: may cause problem with multi-monitors
		mainWindow = new Window(screenSize.width, screenSize.height, false, WINDOW_SCALE);
		// FIXME mainWindow attributes are not updated if the window is resized =/

		System.out.println("Window dimensions: " + mainWindow.getWidth() + " x " + mainWindow.getHeight());

		// Initialization of the stage
		stage.setTitle(game.windowTitle);
		stage.setResizable(mainWindow.isResizable());
		Group group0 = new Group();

		stage.setX(0);
		stage.setY(0);
		stage.setWidth(mainWindow.getWidth());
		stage.setHeight(mainWindow.getHeight());

		stage.setScene(new Scene(group0));
		Canvas canvas = new Canvas(mainWindow.getWidth(), mainWindow.getHeight());
		group0.getChildren().add(canvas);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		stage.show();


		// Initialization of the game
		GameEngine gameEngine = GameEngine.getGameEngine();
		GraphicManager graphicManager = new GraphicManager();


		Initialization.init(game);


		PlayerInput playerInput = new PlayerInput();
		previousPlayerInput = new PlayerInput();

		/*
		 * gameInformation contains the information which is sent to the client each frame.
		 * It is updated each frame by the GameEngine.
		 * Currently not used
		 */
		GameInformation gameInformation = new GameInformation();

		// Getting the player input
		stage.getScene().setOnKeyPressed(playerInput.eventHandler);
		stage.getScene().setOnMousePressed(playerInput.mouseEventHandler);
		stage.getScene().setOnKeyReleased(playerInput.eventHandlerReleased);


		AnimationTimer timer = new AnimationTimer() {
			long before = System.nanoTime();
			float timeToFramerateDisplay = 0f;
			int framerate;

			@Override
			public void handle(long now) {
				/* handle is called in each frame while the timer is active. */

				System.out.print(System.lineSeparator());		// To differentiate the different frames in the console

				System.out.println(playerInput);

				float deltaTime = (now - before) * 0.000000001f;
				System.out.println("Time elapsed since the last frame: " + deltaTime + "s");
				before = now;


				// Updating the game
				gameEngine.update(deltaTime, playerInput, previousPlayerInput, gameInformation);


				previousPlayerInput = playerInput.copy();

				System.out.println("Rendering...");
				graphicManager.renderGame(gc);


				if (FRAMERATE_DISPLAYED) {
					timeToFramerateDisplay -= deltaTime;
					if (timeToFramerateDisplay <= 0) {
						framerate = Math.min(60, (int) (1 / deltaTime));
						timeToFramerateDisplay = 0.1f;
					}
					gc.setFont(Font.font("Helvetica", FontWeight.SEMI_BOLD, 12));
					gc.setFill(Color.LIME);
					gc.fillText(Integer.toString(framerate), 5, 15);
				}
			}
		};

		System.out.println("Starting animation timer.");
		timer.start();
	}



	@Override
	public void stop() {
		/* Is called when the window is closed. */
		System.out.println("\nProgram closed.");

	}



}
