package core.util;

import core.Renderable;
import core.debug.Debug;
import javafx.scene.canvas.GraphicsContext;

/**
 * This class represents a 2-dimensional vector.
 * It contains classic functions on vectors and convenient "constants" to use.
 *
 * @author Raph
 *
 */
public class Vector2 implements Renderable {

	public float x;
	public float y;


	/* Constructor */
	public Vector2(float x, float y) {
		this.x = x;
		this.y = y;
	}



	/* Constants */
	// Ugly, but I don't see any alternative with Java =/
	public static final Vector2 ZERO()	{	return new Vector2(0, 0);	}
	public static final Vector2 UP()	{	return new Vector2(0, 1);	}
	public static final Vector2 DOWN()	{	return new Vector2(0, -1);	}
	public static final Vector2 LEFT()	{	return new Vector2(-1, 0);	}
	public static final Vector2 RIGHT()	{	return new Vector2(1, 0);	}
	public static final Vector2 ONE()	{	return new Vector2(1, 1);	}



	/**
	 * @param toAdd
	 * @return	thisVector + toAdd 		(out-of-place)
	 */
	public Vector2 add(Vector2 toAdd) {
		return new Vector2(this.x + toAdd.x, this.y + toAdd.y);
	}

	/**
	 * @param toSubtract
	 * @return	thisVector - toSubtract 		(out-of-place)
	 */
	public Vector2 minus(Vector2 toSubtract) {
		return new Vector2(this.x - toSubtract.x, this.y - toSubtract.y);
	}

	/**
	 * Applies a translation to this Vector2 (in-place).
	 *
	 * @param translation the translation vector to apply to this Vector2
	 */
	public void translate(Vector2 translation) {
		this.x += translation.x;
		this.y += translation.y;
	}

	/**
	 * @param scalar
	 * @return	scalar * thisVector		(out-of-place)
	 */
	public Vector2 multiply(float scalar) {
		return new Vector2(scalar * x, scalar * y);
	}

	/**
	 * @return	-(thisVector2)		(out-of-place)
	 */
	public Vector2 reverse() {
		return new Vector2(-x, -y);
	}

	/**
	 * @return the normalized Vector2 associated to this Vector2		(out-of-place)
	 */
	public Vector2 normalize() {
		float norm = this.norm();
		return new Vector2(this.x / norm, this.y / norm);
	}


	/**
	 * @return	the norm of this Vector2
	 */
	public float norm() {
		return (float) Math.sqrt(x*x + y*y);
	}

	/**
	 * @param u
	 * @param v
	 * @return the euclidean distance between Vector2 u and Vector2 v
	 */
	public static float distance(Vector2 u, Vector2 v) {
		return (new Vector2(v.x - u.x, v.y - u.y)).norm();
	}

	/**
	 * @param u
	 * @param v
	 * @return the dot product between Vector2 u and Vector2 v
	 */
	public static float dotProduct(Vector2 u, Vector2 v) {
		return u.x * v.x + u.y * v.y;
	}

	/**
	 *
	 * @param u		must not be the zero vector
	 * @param v		must not be the zero vector
	 * @return		the angle in radian between Vector2 u and Vector2 v
	 */
	public static float angle(Vector2 u, Vector2 v) {
		return (float) Math.acos(Vector2.dotProduct(u, v) / (u.norm() * v.norm()));
	}



	@Override
	public void render(GraphicsContext gc, Vector2 position) {
		double windowHeight = gc.getCanvas().getHeight();
		gc.setStroke(Debug.VECTOR_RENDER_COLOR);
		gc.strokeLine(
				position.x,
				windowHeight - position.y,
				position.x + Debug.VECTOR_RENDER_LENGTH_MULTIPLIER * x,
				windowHeight - (position.y + Debug.VECTOR_RENDER_LENGTH_MULTIPLIER * y)
		);

	}




	@Override
	public boolean equals(Object toCompare) {
		if (toCompare.getClass() != getClass()) {
			return false;
		}
		Vector2 toCompareVector = (Vector2) toCompare;
		return (toCompareVector.x == this.x) && (toCompareVector.y == this.y);
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}

}
