package core.util;

import content.GameObject;

/**
 * This class encapsulates information returned about things detected by a raycast.
 * For example, the method GameEngine.raycast() return a RaycastHit object.
 *
 * @author Raph
 *
 */
public class RaycastHit {

	/**
	 * A reference to the GameObject which was hit; null if what was hit isn't a GameObject
	 */
	private GameObject gameObjectHit;

	/**
	 * The distance from the ray origin to the impact point
	 */
	private float distance;

	/**
	 * The normal Vector2 of the line hit
	 */
	private Vector2 normal;

	//? Vector2 impactPoint;		// I don't know if it's useful



	public GameObject getGameObjectHit() {
		return gameObjectHit;
	}

	public float getDistance() {
		return distance;
	}

	public Vector2 getNormal() {
		return normal;
	}



	public RaycastHit(GameObject gameObjectHit, float distance, Vector2 normal) {
		this.gameObjectHit = gameObjectHit;
		this.distance = distance;
		this.normal = normal;
	}



}
