package core.util;

/**
 * A convenient enumeration for the directions.
 *
 * @author Raph
 *
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT,

}
