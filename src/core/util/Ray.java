package core.util;

import core.Renderable;
import core.debug.Debug;
import javafx.scene.canvas.GraphicsContext;

/**
 * This class represents a Ray, which is used to detect collisions with GameObjects.
 * A Ray is always a pure vertical or pure horizontal line, and has a finite length.
 * In practice, what is stored are simply two points.
 *
 * @author Raph
 *
 */
public class Ray implements Renderable {


	private Vector2 originPoint;

	private Vector2 endingPoint;

	private float length;



	public Vector2 getOriginPoint() {
		return originPoint;
	}

	public Vector2 getEndingPoint() {
		return endingPoint;
	}

	public float getLength() {
		return length;
	}



	/**
	 * Constructor using the origin point and the direction from the enumeration.
	 *
	 * @param originPoint
	 * @param direction
	 * @param length
	 *
	 */
	public Ray(Vector2 originPoint, Direction direction, float length) {
		this.originPoint = originPoint;
		this.length = length;
		switch (direction) {
		case UP:
			this.endingPoint = originPoint.add(new Vector2(0, length));
			break;
		case DOWN:
			this.endingPoint = originPoint.add(new Vector2(0, -length));
			break;
		case LEFT:
			this.endingPoint = originPoint.add(new Vector2(-length, 0));
			break;
		case RIGHT:
			this.endingPoint = originPoint.add(new Vector2(length, 0));
			break;
		default:
			break;
		}

	}

	/**
	 * Constructor using the origin point and the direction from a vector.
	 *
	 * @param originPoint
	 * @param direction
	 * @param length
	 *
	 */
	public Ray(Vector2 originPoint, Vector2 direction, float length) {
		this.originPoint = originPoint;
		this.length = length;
		this.endingPoint = originPoint.add(direction.normalize().multiply(length));
	}



	/**
	 * Check if this ray cross a given Collider.
	 * If this is the case, reduce this ray by setting its endingPoint to the intersection point.
	 *
	 * @param collider			the Collider to check collision with
	 * @param colliderOrigin
	 * @return					FIXME the NORMAL between this ray and the collider, or null if there is no intersection
	 */
	public Vector2 collision(Collider collider, Vector2 colliderOrigin) {

		if (collider == null) {
			return null;
		}
		Vector2 result = null;

		int n = collider.getNbPoints();
		for (int i = 0; i < n; i++) {
			Vector2 collider_I = colliderOrigin.add(collider.getPoint(i));
			Vector2 collider_IPlusOne = colliderOrigin.add(collider.getPoint((i+1) % n));

			Vector2 intersectionPoint = Annex.segmentsIntersection(
					originPoint, endingPoint, collider_I, collider_IPlusOne);

			System.out.println("	i = " + i);
			System.out.println("	[" + originPoint + "; " + endingPoint + "] - [" +
					collider_I + "; " + collider_IPlusOne + "]");


			if (intersectionPoint != null) {
				float dist = Vector2.distance(originPoint, intersectionPoint);

				if (dist <= length) {
					// Reducing the ray
					this.endingPoint = intersectionPoint;
					// Updating length accordingly
					length = dist;

					result = Annex.normal(collider_I, collider_IPlusOne, originPoint);
				}
			}
		}
		return result;
	}



	/**
	 * Render this ray in the GraphicContext gc.
	 */
	@Override
	public void render(GraphicsContext gc, Vector2 position) {
		double windowHeight = gc.getCanvas().getHeight();
		gc.setStroke(Debug.RAY_RENDER_COLOR);
		gc.strokeLine(position.x,
				windowHeight - position.y,
				position.x + Debug.RAY_RENDER_LENGTH_MULTIPLIER * (endingPoint.x - position.x),
				windowHeight - (position.y + Debug.RAY_RENDER_LENGTH_MULTIPLIER* (endingPoint.y - position.y)));
	}


	/**
	 * Render this ray from its origin point on the GraphicsContext gc.
	 *
	 * @param gc	the GraphicsContext on which this will be rendered
	 */
	public void render(GraphicsContext gc) {
		render(gc, originPoint);
	}



}
