package core.util;

/**
 * The name says it all.
 * TODO remove this class and update references: SimpleFloatProperty should be used instead!
 *
 * @author Raph
 *
 */
public class MutableFloat {

	public float value;

	public MutableFloat(float value) {
		this.value = value;
	}

}
