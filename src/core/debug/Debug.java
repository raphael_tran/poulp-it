package core.debug;

import javafx.scene.paint.Color;

/**
 * This class contains everything related to the debug aspect of the GameEngine.
 * For example, you can toggle here the display of invisible elements such as colliders or rays.
 */
public abstract class Debug {


	/**
	 * Can any debug element be displayed on screen?
	 */
	public static final boolean DEBUG_ENABLED = true;

	/**
	 * The color to render the vectors, if drawn
	 */
	public static final Color VECTOR_RENDER_COLOR = Color.BLUEVIOLET;

	/**
	 * The factor by which the vectors' length are multiplied before rendering;
	 * helps visualizing short rays.
	 */
	public static final float VECTOR_RENDER_LENGTH_MULTIPLIER = 50f;


	/**
	 * The color to render the colliders, if drawn
	 */
	public static final Color COLLIDER_RENDER_COLOR = Color.GREEN;


	/**
	 * The color to render rays, if drawn
	 */
	public static final Color RAY_RENDER_COLOR = Color.RED;

	/**
	 * The factor by which the rays' length are multiplied before rendering;
	 * helps visualizing short rays.
	 */
	public static final float RAY_RENDER_LENGTH_MULTIPLIER = 5f;









}
