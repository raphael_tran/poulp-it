package core.debug;

import core.Renderable;
import core.util.Vector2;
import javafx.scene.canvas.GraphicsContext;

/**
 * This class represents something to render in a debug view.
 * It encapsulates the element to render and a spatial position.
 * It is useful when you want to visualize things that do not inherently have a position, for example, a Collider or a
 * localized vector.
 *
 * @author Raph
 *
 */
public class DebugRenderable implements Renderable {


	private Renderable element;

	private Vector2 position;



	/* Constructor */
	public DebugRenderable(Renderable element, Vector2 position) {
		this.element = element;
		this.position = position;
	}



	@Override
	public void render(GraphicsContext gc, Vector2 renderPosition) {
		element.render(gc, this.position);
	}

	/**
	 * Convenient method which render this GameObject directly at its current position.
	 *
	 * @param gc	the GraphicsContext on which this will be rendered
	 */
	public void render(GraphicsContext gc) {
		render(gc, this.position);
	}



}
