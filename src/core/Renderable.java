package core;

import core.util.Vector2;
import javafx.scene.canvas.GraphicsContext;

/**
 * Interface implemented by all elements that can be rendered on screen.
 *
 * @author Raph
 *
 */
public interface Renderable {


	/**
	 * Render the element on the given graphic context at the given position.
	 * Anything that should be rendered must override this method.
	 * Use gc.getCanvas().getHeight/Width() to access the size of the canvas if needed.
	 *
	 * If the position to render is obvious, you can add a method: void render(GraphicsContext),
	 * which can be called for standard rendering - ie without needing to specify a position.
	 *
	 * @param gc				the GraphicContext on which this will be rendered
	 * @param renderPosition	the Vector2 giving the position where to draw this element on the GraphicsContext
	 */
	void render(GraphicsContext gc, Vector2 renderPosition);



}
