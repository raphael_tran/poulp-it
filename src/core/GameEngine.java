package core;

import content.GameManager;
import content.GameObject;
import content.Layer;
import content.platformer.Player;
import core.debug.DebugRenderable;
import core.util.Direction;
import core.util.*;
import levels.Level;
import levels.Tile.TileType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Manages the flow of the game; located on the server.
 * Note that this is a singleton class: there can be only one instance of GameEngine.
 *
 * @author Raph
 *
 */
public class GameEngine {


	/**
	 * The list of all GameManagers currently on the scene
	 */
	static LinkedList<GameManager> allGameManagers = new LinkedList<>();

	/**
	 * The list of all GameObjects currently on the scene
	 */
	public static LinkedList<GameObject> allGameObjects = new LinkedList<>();

	/**
	 * A list of all debug elements to render
	 */
	public static LinkedList<DebugRenderable> debugElements = new LinkedList<>();

	/**
	 * A convenient access to the players
	 */
	static Player[] players;


	public static Level level;


	/**
	 * The multiplicative factor used to affect the time flow in the game
	 */
	public static float timeFactor = 1f;

	/**
	 * The length of a tile in window coordinates.
	 * It is changed in order to change the zoom of the camera (not yet...)
	 * XXX
	 */
	public static float tileSize = 32;


	/**
	 * tileReferences[i][j] contains the list of GameObjects which are in the tile (i, j).
	 * null if there is no GameObject in this tile.
	 *
	 * It is used for collision detection.
	 */
	static LinkedList<GameObject>[][] tileReferences;		// TODO actually set it each frame




	/**
	 * This Map associates each TileType with its associated Collider (considering tileSize)
	 */
	public static final Map<TileType, Collider> TILE_TO_COLLIDER = new HashMap<>();

	private static void initializeTILE_TO_COLLIDER() {

		TILE_TO_COLLIDER.put(TileType.EMPTY, null);

		Vector2[] squareColliderArray = {
				Vector2.ZERO(),
				new Vector2(tileSize, 0),
				new Vector2(tileSize, -tileSize),
				new Vector2(0, -tileSize)
		};
		TILE_TO_COLLIDER.put(TileType.SQUARE, new Collider(squareColliderArray));

		Vector2[] triangle1ColliderArray = {
				Vector2.ZERO(),
				new Vector2(tileSize, 0),
				new Vector2(0, -tileSize)
		};
		TILE_TO_COLLIDER.put(TileType.TRIANGLE_TOP_LEFT, new Collider(triangle1ColliderArray));

		Vector2[] triangle2ColliderArray = {
				Vector2.ZERO(),
				new Vector2(tileSize, 0),
				new Vector2(tileSize, -tileSize),
		};
		TILE_TO_COLLIDER.put(TileType.TRIANGLE_TOP_RIGHT, new Collider(triangle2ColliderArray));

		Vector2[] triangle3ColliderArray = {
				Vector2.ZERO(),
				new Vector2(tileSize, -tileSize),
				new Vector2(0, -tileSize)
		};
		TILE_TO_COLLIDER.put(TileType.TRIANGLE_DOWN_LEFT, new Collider(triangle3ColliderArray));

		Vector2[] triangle4ColliderArray = {
				new Vector2(tileSize, 0),
				new Vector2(tileSize, -tileSize),
				new Vector2(0, -tileSize)
		};
		TILE_TO_COLLIDER.put(TileType.TRIANGLE_DOWN_RIGHT, new Collider(triangle4ColliderArray));
	}




	private static GameEngine instance;

	/**
	 * Singleton constructor
	 */
	private GameEngine() {
		initializeTILE_TO_COLLIDER();
	}

	/**
	 * Public interface to get the instance of GameEngine. It creates one if it doesn't already exist.
	 */
	public static GameEngine getGameEngine() {
		if (instance == null) {
			instance = new GameEngine();
		}
		return instance;
	}




	/**
	 * Add a GameObject to the GameEngine.
	 *
	 * @param gameObject	the GameObject to instantiate
	 */
	public static void instantiate(GameObject gameObject) {
		allGameObjects.add(gameObject);
	}

	/**
	 * Remove a GameObject from the GameEngine.
	 *
	 * @param gameObject	the GameObject to remove
	 */
	public static void remove(GameObject gameObject) {
		allGameObjects.remove(gameObject);
	}



	/**
	 * This method is called each frame of the game and updates all game elements.
	 * This also updates the GameInformation object in order to then transmit the new state of the game to the clients.
	 *
	 * @param deltaTime 		the time in seconds it took to complete the last frame
	 * @param playerInput		the current input of the player 		(TODO manage several players)
	 * @param previousPlayerInput
	 * @param gameInformation	the current state of the game
	 *
	 */
	public void update(float deltaTime, PlayerInput playerInput, PlayerInput previousPlayerInput, GameInformation gameInformation) {

		debugElements.clear();

		deltaTime *= timeFactor;

		// Applying all GameManagers
		for (GameManager gameManager: allGameManagers) {
			gameManager.apply();
		}

		// Updating all GameObjects
		for (GameObject gameObject: allGameObjects) {
			gameObject.update(deltaTime, playerInput, previousPlayerInput);
		}

		// Updating the tileReferences matrix
		// TODO

	}





	/**
	 * Cast a ray which can detect obstacles.
	 *
	 * @param rayOrigin		the origin of the ray in absolute coordinates
	 * @param direction		the direction in which the ray is cast
	 * @param length		the length of the ray
	 * @param collisionMask	the Layer on which collisions will be detected
	 *
	 * @return a RaycastHit containing the information about what was hit by the ray.
	 */

	public static RaycastHit raycast(Vector2 rayOrigin, Direction direction, float length, Layer collisionMask) {

		RaycastHit result = null;

		// TODO: we could clamp the length if it it too big for the level

		Ray ray = new Ray(rayOrigin, direction, length);
		debugElements.add(new DebugRenderable(ray, rayOrigin));

		// The coordinates in the grid this ray is casted from
		int[] tileOrigin = toTileCoordinates(rayOrigin);

		// The coordinates in the grid this ray ends
		int[] tileEnding = toTileCoordinates(ray.getEndingPoint());

		System.out.println("Raycast " + direction + " from (" + tileOrigin[0] + ", " + tileOrigin[1] +
				") to (" + tileEnding[0] + ", " + tileEnding[1] + "); length = " + length);


		// Now we traverse the tiles line from tileOrigin to tileEnding.
		// Beware: generic code difficult to read... (but very elegant!)

		// Moving horizontally (0) or vertically (1)?
		int var = (direction == Direction.LEFT || direction == Direction.RIGHT) ? 0 : 1;
		// The index of the column/row which is fixed
		int fixed = tileOrigin[1-var];
		// Moving which way?

		//XXX
		int increment = (direction == Direction.UP || direction == Direction.LEFT) ? -1 : 1;

		for (int k = tileOrigin[var]; increment * (tileEnding[var] - k) >= 0; k += increment) {
			// Setting the current tile
			int currentTileX = (1-var)*k + var*fixed;
			int currentTileY = var*k + (1-var)*fixed;


			// Collisions with other GameObjects	TODO

//			for (GameObject gameObject: tileReferences[currentTileX][currentTileY]) {
//				ray.collision(gameObject);
//			}


			// Collisions with the tile
			System.out.println("current tile: " + currentTileX + ", " + currentTileY);

			TileType tileTypeCurrent = level.getTile(currentTileX, currentTileY);
			if (tileTypeCurrent != TileType.EMPTY) {
				Collider colliderTile = TILE_TO_COLLIDER.get(tileTypeCurrent);
				Vector2 colliderOrigin = toWorldCoordinates(currentTileX, currentTileY);

				System.out.println("Detecting collision between ray and tile collider : " + colliderTile +
						" at tile origin " + colliderOrigin);
				if (colliderTile != null) {
					debugElements.add(new DebugRenderable(colliderTile, colliderOrigin));
				}

				Vector2 normalFromHit = ray.collision(colliderTile, colliderOrigin);
				System.out.println("normalFromHit : " + normalFromHit);

				if (normalFromHit != null) {		// if there is a collision
					System.out.println("result : " + result);
					result = new RaycastHit(null, ray.getLength(), normalFromHit);

					debugElements.add(new DebugRenderable(normalFromHit, colliderOrigin));
					// XXX LAST TILE ???

				}
			}
		}
		return result;
	}


	/**
	 * @param A
	 * @return	the grid coordinates corresponding with the position of the point A
	 */
	private static int[] toTileCoordinates(Vector2 A) {
		int[] result = {
				(int) Math.floor(A.x / tileSize),
				(int) Math.floor((Launcher.mainWindow.getHeight() - A.y) / tileSize)
		};
		return result;
	}

	/**
	 * @param xTile
	 * @param yTile
	 * @return	the origin point of the tile (x, y) (i.e. its top-left corner) in absolute coordinates
	 */
	private static Vector2 toWorldCoordinates(int xTile, int yTile) {
		return new Vector2(
				xTile * tileSize,
				(Launcher.mainWindow.getHeight() - yTile * tileSize)
		);
	}

	public static Vector2 screenToWindow(Vector2 screenPos) {
		return new Vector2(screenPos.x, (float) Launcher.mainWindow.getHeight() - screenPos.y);

	}










}
