package core;

import content.GameObject;
import content.alien.Alien;
import content.alien.Pineapple;
import content.maze.PlayerMaze;
import content.platformer.Player;
import content.rhythm_game.RhythmConductor;
import content.shooter.PlayerShooter;
import core.util.Vector2;
import javafx.scene.image.Image;
import levels.Level;

import java.io.IOException;
import java.util.LinkedList;

/**
 * This class stores the code to initialize the different games that can be set in the Launcher.
 *
 * The functioning of this class is not very clean, notably with its constant access of GameEngine attributes,
 * but it's still preferable since there are many different initialization methods (due to the different demonstration
 * games).
 * If you were to use Poulp'IT to make one game, you could just put one initialization function in the GameEngine class.
 */
public final class Initialization {


	public static void init(Game game) throws IOException {

		Window window = Launcher.mainWindow;


		/*
		 * The structure is the following:
		 * each case contains the initialization code for one game
		 * and is divided in two parts: the variables of the initialization, then the initialization.

		 */
		switch (game) {

		case HOOK_BATTLE:

			/* Initialization parameters */

			int nbPlayers = 1;
			String levelName = "level0";

			GraphicManager.background = new Image(Launcher.RESOURCES_URI + "graphic/backgrounds/curtains.jpg",
					window.getWidth(), window.getHeight(), true, true);


			/* Initialization */

			// Importing the level
			System.out.println("Beginning level importation: " + levelName);
			GameEngine.level = new Level("levels/" + levelName + ".txt");

			// Initializing the tileReferences matrix
			GameEngine.tileReferences = (LinkedList<GameObject>[][]) new LinkedList[50][50];		// XXX
			for (int i = 0; i < GameEngine.tileReferences.length; i++) {
				for (int j = 0; j < GameEngine.tileReferences[i].length; j++) {
					GameEngine.tileReferences[i][j] = new LinkedList<>();
				}
			}

			// Instantiating players and adding them to the players array
			System.out.println("Instantiating players...");
			GameEngine.players = new Player[nbPlayers];
			for (int i = 0; i < nbPlayers; i++) {
				Vector2 spawnPosition;
				//			spawnPosition = new Vector2((float)Launcher.WINDOW_WIDTH / 2, (float) Launcher.WINDOW_HEIGHT / 2);
				//			spawnPosition.translate(Vector2.RIGHT().multiply(100 * i));
				//			spawnPosition = new Vector2(280, 710);
				spawnPosition = new Vector2(585, 930);
				Player playerI = new Player(spawnPosition, 10);
				GameEngine.players[i] = playerI;
				GameEngine.instantiate(playerI);
			}
			System.out.println("Players instantiation finished");

			// ----

			break;



		case SHOOTER:

			/* Initialization parameters */

			String levelName2 = "square";


			/* Initialization */

			// Importing the level
			System.out.println("Beginning level importation: " + levelName2);
			GameEngine.level = new Level("levels/" + levelName2 + ".txt");

			PlayerShooter playerShooter = new PlayerShooter(new Vector2(585, 730));
			GameEngine.instantiate(playerShooter);


			break;



		case MAZE:

			/* Initialization parameters */

			int mazeWidth = 40;
			int mazeHeight = 20;
			boolean fantasticMode = false;


			/* Initialization */

			// Note: the maze object is actually part of the player Object
			PlayerMaze playerMaze = new PlayerMaze(mazeWidth, mazeHeight, fantasticMode);
			GameEngine.instantiate(playerMaze);


			break;



		case ALIEN:

			/* Initialization parameters */

			int nbPineapples = 30;

			GraphicManager.background = new Image(Launcher.RESOURCES_URI + "graphic/alien/space.jpg",
					window.getWidth(), window.getHeight(), true, true);


			/* Initialization */

			Alien alien = new Alien(new Vector2(100, 100), null, null);
			GameEngine.instantiate(alien);
			for (int i = 0; i < nbPineapples ; i++) {
				Vector2 position = new Vector2(
						(float) (window.getWidth() * Math.random()),
						(float) (window.getHeight() * Math.random())
				);
				GameEngine.instantiate(new Pineapple(position, null, null));
			}


			break;


		case RHYTHM_GAME:

			/* Initialization parameters */

			GraphicManager.background = new Image(Launcher.RESOURCES_URI + "graphic/backgrounds/background_rhythm_game.png",
					window.getWidth(), window.getHeight(), true, true);


			/* Initialization */

			RhythmConductor conductor = new RhythmConductor();
			GameEngine.instantiate(conductor);


			break;



		default:

			break;
		}



	}




}
