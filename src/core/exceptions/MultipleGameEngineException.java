package core.exceptions;

import core.GameEngine;

/**
 * Exception raised when trying to instantiate several GameEngines.
 * (actually probably useless =/)
 * EDIT from the future: I discovered the concept of singleton class, so yeah, it was really useless.
 *
 * @author Raph
 * @see GameEngine
 *
 */

@SuppressWarnings("serial")
@Deprecated
public class MultipleGameEngineException extends Exception {

	public MultipleGameEngineException() {
		super("One instance of GameEngine already exists");
	}

}
