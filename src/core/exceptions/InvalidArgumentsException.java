package core.exceptions;

/**
 *
 * @author Raph
 *
 */

@SuppressWarnings("serial")
@Deprecated
public class InvalidArgumentsException extends Exception {

	public InvalidArgumentsException(String msg) {
		super(msg);
	}

}
