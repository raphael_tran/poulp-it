package core.annotations;

/**
 * Annotation to use on an element which is not used.
 * There is no code here but I wanted to be able to mark Unused on things.
 *
 * @author Raph
 *
 */
public @interface Unused {

}
