![Poulp'IT](https://gitlab.com/raphael_tran/poulp-it/-/raw/master/Logo.png "Logo Poulp'IT")


Poulp'IT is a Java game engine created as part of a school project at Télécom SudParis.
It relies on the [JavaFX](https://openjfx.io/) library.
It provides a framework to work with by programming in Java, but does not offer a graphical interface.

You can check out [here](https://docs.google.com/presentation/d/1yhxJNlDnptAMP0mfMwzg2vcQ_UpCcy0HHZIVjs0mFk4/edit?usp=sharing)
the slides used for the final presentation of the project if you want a visual explanation of the engine (in french).


## History ##

This game engine was created as part of a one semester-long school project in the engineering school Télécom SudParis.
Initially, we planned to create a game from scratch but, as we were advancing the underlying code foundations of the
game, we realized our code was generic enough to allow the creation of a vast range of games, so we decided to shift the
focus of our project on the development of a game engine.
The functioning of Poulp'IT is strongly inspired by the functioning of Unity, and uses a [component-based paradigm](
https://en.wikipedia.org/wiki/Component-based_software_engineering).

Even though there is little reason to use this game engine for a real project, building it proved really instructive.
The project was an opportunity to use all the advanced concept of Java on a large scale, and I learned a lot in
terms of software development, software architecture and game engine structure.
Also, besides the code itself, the project called for other skills, such as project and team management, and required 
good programming practices. For example, we managed to have the source code comply with Javadoc conventions. 
Programming a game engine from scratch proved very instructive on underlying techniques used in games, such as 
raycasting.


## File structure ##

Here is a description of the different folders of the program. See the next paragraph for more details.
  * content : contains the higher-level game element (for example: skeleton of GameObject class, Player class),  
                and sub-folders for the content of the demonstration games.
  * core : everything that run the game engine at a low level.  
            There are notably the Launcher, the GameEngine et the GraphicManager.
    * core.exceptions : custom exceptions created
    * core.util : utility classes (for example: Vector2)
  * resources : non-code resources; typically, sprites and audio


## Core functioning ##

  * The Launcher class (located in the core folder) is the entry point of the program.
  * The Launcher initializes the program, the window and create a GameEngine and a GraphicManager instances.  
    An Animation Timer take care of executing the GameEngine each frame and displaying what's needed on the screen
    through the GraphicManager.
  * Each frame, the GameEngine is updated, which means :  
        - it applies the effects of all GameManager,  
        - then it updates the GameObjects.  
  * GameManager is the interface implemented by any element managing an "hidden" aspect of the game (for example, the
        management of the music)       
  * GameObject is the superclass for all game elements that:
        - have a spatial position
        - have an appearance (through a render() method)  
    To a GameObject can be attached a list of scripts which characterize its behaviour.  
    By default, updating a GameObject means updating all its scripts, but this can be customized by overriding the update
    method.
  * A script is an object which inherit from the MonoBehaviour class. It notably has the methods start() and update().


  The project is globally well documented, so you can view the documentation of any specific item for precise details.


## Installation and running the demos ##

The only dependency (excepted Java) is to have the [JavaFX library](https://openjfx.io/).  
There are currently 4 demo games. To choose one, change the static attribute Game in the Launcher class.
You can check out the underlying code as you wish. Remember that the high level stuff is always in the content folder.
It is possible to make a game without dealing with the core of the engine.
Please note that the demo are very buggy since they were created very quickly to showcase features.

Be sure to add this to the VM options if you are running the program in an IDE.
--module-path "path to JavaFX lib folder" --add-modules javafx.controls, javafx.fxml, javafx.media

## Screenshots

![Maze game illustration](https://gitlab.com/raphael_tran/poulp-it/-/raw/master/images/maze.png "Maze Game")

![Platformer game illustration](https://gitlab.com/raphael_tran/poulp-it/-/raw/master/images/platformer.png "Platformer Game")

![Rhythm game illustration](https://gitlab.com/raphael_tran/poulp-it/-/raw/master/images/rhythm.png "Rhythm Game")


## Creating your own game

To create your own game, you need to go to the core package, add an element to the Game enumeration, and set the Game
attribute in the launcher to be yours. Alternatively, you can get rid of the enumeration and simply tweak the Launcher
so that there is only one game.  

To initialize your game, you can simply add your tasks in the Initialization class like the other games.


## Authors

[Raphaël Tran](http://raphael-tran.fr) - Programming  
[Matthias Goffette](https://matthias4217.github.io) - Programming  
[Toni Oriol](https://tonioriol.work/) - Graphics  
Sébastien Collard  


## License

This game engine is [public domain (CC0)](http://creativecommons.org/publicdomain/zero/1.0).
Obviously, attribution is appreciated, but do whatever you want.
